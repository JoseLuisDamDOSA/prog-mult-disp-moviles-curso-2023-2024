﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03___WindowsFormsApplicationBaraja
{
    class Baraja
    {
        // Atributos de la clase
        private int[] tabla;       // Tabla con elementos
        private int nElementos;    // Cantidad actual de elementos

        public Baraja()
        {
            tabla = new int[12];
            nElementos = 0;
        }

        public void Inicializar()
        {
            Random r = new Random();

            nElementos = 0;
            for (int i = 0; i < 12; i++)
            {
                bool encontrado;
                int n;
                do
                {
                    n = r.Next(1, 13);
                    encontrado = false;
                    for (int j = 0; (j < i) && (!encontrado); j++)
                        if (tabla[j] == n)
                            encontrado = true; 
                } while (encontrado);
                tabla[i] = n;
                nElementos++;
            }
        }

        public void Vaciar()
        {
            for (int i = 0; i < nElementos; i++)
                tabla[i] = 0;
            nElementos = 0;
        }

        public bool Vacia()
        {
            if (nElementos == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
       }

        public int Cantidad()
        {
            return nElementos;
        }

        public int Extraer()
        {
            if (!Vacia())
            {
                Random r = new Random();
                int n = r.Next(0, nElementos);
                int elemento;

                elemento = tabla[n];
                for(int i=n;i<nElementos-1;i++)
                {
                    tabla[i] = tabla[i + 1];
                }
                tabla[nElementos-1] = 0;
                nElementos--;
                return elemento;
            }
            else
                return -1;
        }

        public String Escribir()
        {
            String nueva = "";
            for(int i=0;i<nElementos;i++)
            {
                if (i != nElementos - 1)
                    nueva += tabla[i] + " - ";
                else
                    nueva += tabla[i];
            }
            return nueva;
        }



    }
}
