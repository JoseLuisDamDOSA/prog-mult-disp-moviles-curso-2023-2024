﻿namespace _03___WindowsFormsApplicationBaraja
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonInicializar = new System.Windows.Forms.Button();
            this.labelJ1C1 = new System.Windows.Forms.Label();
            this.labelJ1C2 = new System.Windows.Forms.Label();
            this.labelJ2C1 = new System.Windows.Forms.Label();
            this.labelJ2C2 = new System.Windows.Forms.Label();
            this.textBoxJ1PuntosParcial = new System.Windows.Forms.TextBox();
            this.textBoxJ2PuntosParcial = new System.Windows.Forms.TextBox();
            this.textBoxJ1PuntosTotal = new System.Windows.Forms.TextBox();
            this.textBoxJ2PuntosTotal = new System.Windows.Forms.TextBox();
            this.buttonJugar = new System.Windows.Forms.Button();
            this.textBoxBaraja = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonInicializar
            // 
            this.buttonInicializar.Location = new System.Drawing.Point(138, 266);
            this.buttonInicializar.Name = "buttonInicializar";
            this.buttonInicializar.Size = new System.Drawing.Size(131, 72);
            this.buttonInicializar.TabIndex = 1;
            this.buttonInicializar.Text = "Inicializar";
            this.buttonInicializar.UseVisualStyleBackColor = true;
            this.buttonInicializar.Click += new System.EventHandler(this.buttonInicializar_Click);
            // 
            // labelJ1C1
            // 
            this.labelJ1C1.AutoSize = true;
            this.labelJ1C1.ForeColor = System.Drawing.Color.Blue;
            this.labelJ1C1.Location = new System.Drawing.Point(48, 158);
            this.labelJ1C1.Name = "labelJ1C1";
            this.labelJ1C1.Size = new System.Drawing.Size(70, 25);
            this.labelJ1C1.TabIndex = 2;
            this.labelJ1C1.Text = "label1";
            this.labelJ1C1.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelJ1C2
            // 
            this.labelJ1C2.AutoSize = true;
            this.labelJ1C2.ForeColor = System.Drawing.Color.Blue;
            this.labelJ1C2.Location = new System.Drawing.Point(133, 158);
            this.labelJ1C2.Name = "labelJ1C2";
            this.labelJ1C2.Size = new System.Drawing.Size(70, 25);
            this.labelJ1C2.TabIndex = 2;
            this.labelJ1C2.Text = "label1";
            this.labelJ1C2.Click += new System.EventHandler(this.labelJ1C2_Click);
            // 
            // labelJ2C1
            // 
            this.labelJ2C1.AutoSize = true;
            this.labelJ2C1.ForeColor = System.Drawing.Color.Blue;
            this.labelJ2C1.Location = new System.Drawing.Point(403, 158);
            this.labelJ2C1.Name = "labelJ2C1";
            this.labelJ2C1.Size = new System.Drawing.Size(70, 25);
            this.labelJ2C1.TabIndex = 2;
            this.labelJ2C1.Text = "label1";
            this.labelJ2C1.Click += new System.EventHandler(this.label3_Click);
            // 
            // labelJ2C2
            // 
            this.labelJ2C2.AutoSize = true;
            this.labelJ2C2.ForeColor = System.Drawing.Color.Blue;
            this.labelJ2C2.Location = new System.Drawing.Point(479, 158);
            this.labelJ2C2.Name = "labelJ2C2";
            this.labelJ2C2.Size = new System.Drawing.Size(70, 25);
            this.labelJ2C2.TabIndex = 2;
            this.labelJ2C2.Text = "label1";
            // 
            // textBoxJ1PuntosParcial
            // 
            this.textBoxJ1PuntosParcial.Location = new System.Drawing.Point(66, 198);
            this.textBoxJ1PuntosParcial.Name = "textBoxJ1PuntosParcial";
            this.textBoxJ1PuntosParcial.Size = new System.Drawing.Size(100, 31);
            this.textBoxJ1PuntosParcial.TabIndex = 3;
            this.textBoxJ1PuntosParcial.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxJ2PuntosParcial
            // 
            this.textBoxJ2PuntosParcial.Location = new System.Drawing.Point(408, 198);
            this.textBoxJ2PuntosParcial.Name = "textBoxJ2PuntosParcial";
            this.textBoxJ2PuntosParcial.Size = new System.Drawing.Size(100, 31);
            this.textBoxJ2PuntosParcial.TabIndex = 3;
            this.textBoxJ2PuntosParcial.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxJ1PuntosTotal
            // 
            this.textBoxJ1PuntosTotal.Location = new System.Drawing.Point(70, 41);
            this.textBoxJ1PuntosTotal.Name = "textBoxJ1PuntosTotal";
            this.textBoxJ1PuntosTotal.Size = new System.Drawing.Size(100, 31);
            this.textBoxJ1PuntosTotal.TabIndex = 3;
            this.textBoxJ1PuntosTotal.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxJ2PuntosTotal
            // 
            this.textBoxJ2PuntosTotal.Location = new System.Drawing.Point(397, 52);
            this.textBoxJ2PuntosTotal.Name = "textBoxJ2PuntosTotal";
            this.textBoxJ2PuntosTotal.Size = new System.Drawing.Size(100, 31);
            this.textBoxJ2PuntosTotal.TabIndex = 3;
            this.textBoxJ2PuntosTotal.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonJugar
            // 
            this.buttonJugar.Location = new System.Drawing.Point(325, 266);
            this.buttonJugar.Name = "buttonJugar";
            this.buttonJugar.Size = new System.Drawing.Size(131, 72);
            this.buttonJugar.TabIndex = 1;
            this.buttonJugar.Text = "Jugar";
            this.buttonJugar.UseVisualStyleBackColor = true;
            this.buttonJugar.Click += new System.EventHandler(this.buttonJugar_Click);
            // 
            // textBoxBaraja
            // 
            this.textBoxBaraja.ForeColor = System.Drawing.Color.Blue;
            this.textBoxBaraja.Location = new System.Drawing.Point(66, 102);
            this.textBoxBaraja.Name = "textBoxBaraja";
            this.textBoxBaraja.Size = new System.Drawing.Size(431, 31);
            this.textBoxBaraja.TabIndex = 4;
            this.textBoxBaraja.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxBaraja.TextChanged += new System.EventHandler(this.textBoxBaraja_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 527);
            this.Controls.Add(this.textBoxBaraja);
            this.Controls.Add(this.textBoxJ2PuntosTotal);
            this.Controls.Add(this.textBoxJ1PuntosTotal);
            this.Controls.Add(this.textBoxJ2PuntosParcial);
            this.Controls.Add(this.textBoxJ1PuntosParcial);
            this.Controls.Add(this.labelJ2C2);
            this.Controls.Add(this.labelJ2C1);
            this.Controls.Add(this.labelJ1C2);
            this.Controls.Add(this.labelJ1C1);
            this.Controls.Add(this.buttonJugar);
            this.Controls.Add(this.buttonInicializar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonInicializar;
        private System.Windows.Forms.Label labelJ1C1;
        private System.Windows.Forms.Label labelJ1C2;
        private System.Windows.Forms.Label labelJ2C1;
        private System.Windows.Forms.Label labelJ2C2;
        private System.Windows.Forms.TextBox textBoxJ1PuntosParcial;
        private System.Windows.Forms.TextBox textBoxJ2PuntosParcial;
        private System.Windows.Forms.TextBox textBoxJ1PuntosTotal;
        private System.Windows.Forms.TextBox textBoxJ2PuntosTotal;
        private System.Windows.Forms.Button buttonJugar;
        private System.Windows.Forms.TextBox textBoxBaraja;
    }
}

