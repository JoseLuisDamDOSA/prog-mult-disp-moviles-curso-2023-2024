﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03___WindowsFormsApplicationBaraja
{
    public partial class Form1 : Form
    {
        private Baraja miBaraja;

        public Form1()
        {
            InitializeComponent();
            miBaraja = new Baraja();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonInicializar_Click(object sender, EventArgs e)
        {
            miBaraja.Inicializar();
            textBoxBaraja.Text = miBaraja.Escribir();
            textBoxJ1PuntosParcial.Text = "0";
            textBoxJ2PuntosParcial.Text = "0";
            textBoxJ1PuntosTotal.Text = "0";
            textBoxJ2PuntosTotal.Text = "0";
            labelJ1C1.Text = "";
            labelJ1C2.Text = "";
            labelJ2C1.Text = "";
            labelJ2C2.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
             
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonJugar_Click(object sender, EventArgs e)
        {
            labelJ1C1.Text = miBaraja.Extraer().ToString();
            labelJ1C2.Text = miBaraja.Extraer().ToString();
            labelJ2C1.Text = miBaraja.Extraer().ToString();
            labelJ2C2.Text = miBaraja.Extraer().ToString();
            textBoxBaraja.Text = miBaraja.Escribir();

            // Contabilizar la jugada en los marcadores parciales
            int p1 = Int32.Parse(labelJ1C1.Text) + Int32.Parse(labelJ1C2.Text);
            textBoxJ1PuntosParcial.Text = p1.ToString();
            int p2 = Int32.Parse(labelJ2C1.Text) + Int32.Parse(labelJ2C2.Text);
            textBoxJ2PuntosParcial.Text = p2.ToString();

            // Contabilizar la jugada en los marcadores totales
            if (p1 > p2)
            {
                textBoxJ1PuntosTotal.Text =
                    (Int32.Parse(textBoxJ1PuntosTotal.Text) + 1).ToString();
            }
            else
            {
                if (p2 > p1)
                {
                    textBoxJ2PuntosTotal.Text =
                        (Int32.Parse(textBoxJ2PuntosTotal.Text) + 1).ToString();
                }
                else
                {
                    textBoxJ1PuntosTotal.Text =
                        (Int32.Parse(textBoxJ1PuntosTotal.Text) + 1).ToString();
                    textBoxJ2PuntosTotal.Text =
                        (Int32.Parse(textBoxJ2PuntosTotal.Text) + 1).ToString();
                }
            }
        }

        private void labelBaraja_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void labelJ1C2_Click(object sender, EventArgs e)
        {

        }

        private void textBoxBaraja_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
