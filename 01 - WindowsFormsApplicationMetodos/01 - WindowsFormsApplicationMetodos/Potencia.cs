﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01___WindowsFormsApplicationMetodos
{
    class Potencia
    {
        private int baseP;
        private int exp;

        public Potencia()
        {
            baseP = 1;
            exp = 0; 
        }

        public Potencia(int b,int e)
        {
            baseP = b;
            exp = e;
        }

        public int GetBase()
        {
            return baseP;
        }

        public int GetExponente()
        {
            return exp;
        }

        public void SumarBase(int nuevaBase)
        {
            baseP = baseP + nuevaBase;
            nuevaBase = baseP;
        }

        public void SumarBase(int nueva1,int nueva2)
        {
            baseP = baseP + nueva1 + nueva2;
        }


        public void SumarBaseRef(ref int nuevaBase)
        {
            baseP = baseP + nuevaBase;
            nuevaBase = baseP;
        }

        public void SumarBaseOut(out int nuevaBase)
        {
            baseP = baseP + 8;
            nuevaBase = baseP;
        }
    }
}
