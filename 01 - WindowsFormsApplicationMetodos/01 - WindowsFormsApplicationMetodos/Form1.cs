﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01___WindowsFormsApplicationMetodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Potencia p;
            int numero;

            p = new Potencia();
            labelBase.Text = p.GetBase().ToString();
            labelExponente.Text = p.GetExponente().ToString();

            // Llamada a un método con parámetro de entrada
            /*
            numero = 3;
            labelResultado.Text += "El numero vale " + numero;
            p.SumarBase(numero);
            labelResultado.Text += "El numero vale " + numero;
           */

            // Llamada a un método con parámetro de referencia
            /*
            numero = 3;
            labelResultado.Text += "El numero vale " + numero;
            p.SumarBaseRef(ref numero);
            labelResultado.Text += "El numero vale " + numero;
            */

            // Llamada a un método con parámetro de salida
            numero = 1;
            labelResultado.Text += "El numero vale " + numero;
            p.SumarBaseOut(out numero);
            labelResultado.Text += "El numero vale " + numero;
        }
    }
}
